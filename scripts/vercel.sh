#!/usr/bin/env bash

pip install mkdocs-material
pip install mkdocs-minify-plugin
pip install mkdocs-redirects
mkdocs build --site-dir public
